var gulp         = require('gulp');
var uglify       = require('gulp-uglify');
var clean        = require('gulp-clean');
var csso         = require('gulp-csso');
var pkg          = require('../../package.json');
var srcPath      = pkg['basePath']['stage'];
var dstPath      = pkg['basePath']['deliverable'];

gulp.task('deliverable', ['build'], function () {
  
  return gulp.src(dstPath)
    .pipe(clean())
    .on('end', function () {
      
      gulp.src(srcPath + '/js/**')
        .pipe(uglify())
        .pipe(gulp.dest(dstPath + '/js'));
      
      gulp.src(srcPath + '/css/**')
        .pipe(csso())
        .pipe(gulp.dest(dstPath + '/css'));
      
      gulp.src(srcPath + '/**/*.html')
        .pipe(gulp.dest(dstPath));
      
      gulp.src( srcPath + '/fonts/**/*.{ttf,eot,woff,svg}')
        .pipe(gulp.dest(dstPath + '/fonts'));
      
      gulp.src( srcPath + '/img/**/*.{jpg,jpeg,png,gif,svg}')
        .pipe(gulp.dest(dstPath + '/img'));      
    });
});
