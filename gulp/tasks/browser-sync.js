var gulp          = require('gulp');
var browserSync   = require('browser-sync');
var pkg           = require('../../package.json');
var dstPath       = pkg['basePath']['stage'];

gulp.task('browser-sync', function() {

  setTimeout(function(){
    browserSync.init([
      dstPath + '/**/*.html',
      dstPath + '/**/*.css',
      dstPath + '/**/*.js',
      dstPath + '/**/*.json',
      dstPath + '/**/*.{png,jpg,gif,svg}'
    ], {
      server: {
        baseDir:  dstPath
      }
    });
  },5000);

});

