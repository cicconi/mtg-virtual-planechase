var less         = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var gulp         = require('gulp');
var handleErrors = require('../util/handleErrors');
var pkg          = require('../../package.json');
var srcPath      = pkg['basePath']['application'];
var dstPath      = pkg['basePath']['stage'];

gulp.task('less', function() {
  return gulp.src( srcPath + '/less/main.less')
  .pipe(less({ 
    paths: [ srcPath + '/.bower_components'],
    sourceMap: true,
    outputSourceFiles: true
  }))
  .on('error', handleErrors)
  .pipe(autoprefixer('last 2 versions', 'Explorer >= 7'))
  .pipe(gulp.dest( dstPath + '/css'));
});
