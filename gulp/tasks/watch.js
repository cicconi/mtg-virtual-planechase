var gulp          = require('gulp');
var watch         = require('gulp-watch');
var pkg           = require('../../package.json');
var srcPath       = pkg['basePath']['application'].replace(/^\.\/+/, '');//O gulp-watch requer setar o path sem o ./
var dstPath       = pkg['basePath']['stage'];

gulp.task('watch', ['build'], function() {
  gulp.watch( srcPath + '/less/**/*.less',     ['less']);
  gulp.watch( srcPath + '/js/**/*.js',         ['browserify']);
  gulp.watch( srcPath + '/img/**/*.{png,jpg,gif,svg}', ['images']);
  gulp.watch( srcPath + '/**/*.html',          ['htmls']);
  gulp.watch( srcPath + '/json/**/*.json',     ['jsons']);
});
