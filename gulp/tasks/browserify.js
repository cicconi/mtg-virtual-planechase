var browserify   = require('browserify');
var gulp         = require('gulp');
var handleErrors = require('../util/handleErrors');
var source       = require('vinyl-source-stream');
var pkg          = require('../../package.json');
var srcPath      = pkg['basePath']['application'];
var dstPath      = pkg['basePath']['stage'];

gulp.task('browserify', function(){
  return browserify({
      entries: [srcPath + '/js/main.js']
    })
    .bundle({debug: true})
    .on('error', handleErrors)
    .pipe(source('main.js'))
    .pipe(gulp.dest(dstPath + '/js'));
});
