var gulp       = require('gulp');
var changed    = require('gulp-changed');
var path       = require('path');
var pkg        = require('../../package.json');
var srcPath    = pkg['basePath']['application'];
var dstPath    = pkg['basePath']['stage'];


module.exports = function (from, to) {
  return function () {

    var src = path.join(srcPath, from);
    var mirror = path.dirname(from).replace(/\*/g,'');
    var dst = path.join(dstPath, to || mirror);

    console.log('\n     _.-"""-.          ');
    console.log('   ." \\      `".      ');
    console.log('  /  .-"---._   \\     copyboy!');
    console.log('  |_/  _   _ `\\_|     ');
    console.log('  / |  o   o  | \\     from: ', src);
    console.log('  \\/     7     \\/     to:   ', dst);
    console.log('   \\   .___.   /      ');
    console.log('    \'._  _  _.\'      ');
    console.log('       )   (           ');

    return gulp.src(src)
      .pipe(changed(dst))
      .pipe(gulp.dest(dst));
  }
}
