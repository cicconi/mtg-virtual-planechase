var _         = require('lodash');
var rsync     = require("rsyncwrapper").rsync;

var common = {
  args: ['--verbose'],
  recursive: true,
  exclude: [
    '.git*', 
    'node_modules', 
  ],
};

module.exports = function(password, env) {

  console.log('Password hint: ', password);

  var config = _.merge(env, common);

  rsync(config, function() {});
};