# Planechase Plataform

## Intro

Storyboard do aplicativo.

Ideias reunidas buscando um sentido quando colocadas em ordem.
As tabulações representam a hierarquia cronométrica das ações.

Legenda:

* ? - ideia
* . - ação tomada pelo usuário
* \* - resultado da ação do usuário
* -> - comportamento

## Definições

* ? narração
* ? dublagem

. definir o deck

. começar a partida
  * gira o primeiro plano
    -> efeito na aparição do plano
    -> tela travada no plano
    ? temporizador com quantidade de tempo naquele plano
  
  . lançar o dado

  * gira blank no dado
    -> ambiente misterioso (transições esmaecentes)
    -> modal com frase do tipo: "girar o dado novamente?", "pay!", "custará mana!"
    -> que deve sumir em alguns segundos. Talvez colocar alguma ampulheta.
    -> e botão como opção de jogar o dado novamente. Deve haver apelo para girar o dado
  * gira chaos no dado
    -> ambiente caótico (letras tremendo)
    -> efeito anunciando Chaos acompanhado de som ou vibração
    -> efeito de zoom na frase (se tiver tradução pra PT, melhor)
  * gira plane no dado
    -> ambiente brilhante
    -> efeito anunciando novo Plano
    -> efeito de zoom na frase

  * gita um novo plano