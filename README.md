# MTG Virtual Planechase

## TO DO
* Dado
    - alavanca? roleta? pião da casa própria?
    * [raphael.js](http://raphaeljs.com/) - lib js para trabalhar com vetor
    * [matter.js](http://brm.io/matter-js/) - lib opensource para trabalhar com física em JS
    * referência de [roleta](http://tpstatic.com/_sotc/sites/default/files/1010/source/roulettewheel.html). Acompanha um [how to](http://tech.pro/tutorial/1008/creating-a-roulette-wheel-using-html5-canvas)
    * roleta estilo [mario kart](http://demo.st-marron.info/roulette/sample/demo.html)
    * lançador de [dados em webgl](http://diox.github.io/webdiceroller/)
    * outro dado [tridimensional](http://mamisab.chez-alice.fr/cube/dicewthprs.htm), com fundo [transparente](http://mamisab.chez-alice.fr/cube/dice.htm) também
* parallax quando mover o plane (no ipad)
* shake na ação
* controle remoto?


## MTG na Web
* [mtgjson.com](http://mtgjson.com) - fonte, em formato JSON, de todas as cartas de MTG (até Born of Gods)
* [mtgimage.com](http://mtgimage.com) - serviço que retorna imagem da carta através do nome, set/nome, ou multiverseId
* [Símbolos](http://www.mediafire.com/folder/92vnddvuu7ian/BaconCatBug's_SVG_Expansion_Symbol_Pack) de mana e edições em SVG e PNG.


## Outros Planechase Simulators
* [Online Planechase Deck Simulator](http://planechase-deck.tumblr.com/)
* [Marks Webseite Planechase Simulator](http://mark.tvk.rwth-aachen.de/software/decksimulator/planechase/planechase.html)
* [humpheh Planechase Simulator](http://humpheh.com/magic/p/)


## DONE
* Substituir os caracteres {T} por símbolos
* Pesquisar por fontes de informação
* Alimentar base de dados não relacional
* Carregar dados dos Planos em interface
* Assets colocados no [Dropbox](https://www.dropbox.com/sh/s496sojqcyoo60t/9PB2Xt3yD_)