var $ = jQuery =  require('jquery');
var _ =           require('lodash');
var symbolize =   require('./symbolize');
var promise =     require('./promise');
var config =      require('./config');
var library =     require('./library')();
var $el;

/*
 * Faz download do template da carta (com cache).
 */
function getPlaneTemplate () {
  return promise('planeTemplate', function() {
    return $.ajax(config.route.template);
  });
}

/*
 * Recebe o texto do Plane e divide em duas
 * partes: Oracle (regra do plano) e Chaos;
 * transforma os símbolos de mana.
 */
function parseOracle (text) {
  var parsed = text.split('Whenever you roll');
  return {
    oracle: symbolize( italicizeBrackets(parsed[0]).replace(/\n/g,'<br>') ),
    chaos: symbolize( italicizeBrackets('Whenever you roll' + parsed[1]) )
  }
}

/*
 * Recebe um texto e coloca tags <em> em volta de
 * qualquer sentença entre parênteses.
 */
function italicizeBrackets (text) {
  return text.replace(/(\([\w\d\s,.:;+/'']*\))/gi, '<em>$1</em>');
}

/*
 * Devole markup da carta populada.
 */
// function renderCard (cardObj) {
function renderCard (response) {
  console.log(response);
  var cardObj = _.extend(response[0], {
    next_multiverseid: response[1] && response[1].multiverseid || 0
  });
  return getPlaneTemplate().then(function (template) {
    return _.template(template, _.extend(cardObj, parseOracle(cardObj.text)));
  });
}

/*
 * Apaga o plano atual [em tela].
 */
function exitPlane () {
  var deferred = new $.Deferred();
  _.delay(function(){
    $el.empty();
    deferred.resolve();
  },1);
  // $el.fadeOut(function() {
  //   $el.empty();
  //   deferred.resolve();
  // });
  return deferred.promise();
}

/*
 * Escreve 'response' no battlefield.
 */
function enterPlane (response) {
  var deferred = new $.Deferred();
  _.delay(function(){
    $el.hide().empty().append(response).show();
    deferred.resolve(response);
  },1);
  // $el.hide().empty().append(response).fadeIn(function () {
  //   deferred.resolve(response);
  // });
  return deferred.promise();
}

/*
 * Renderiza o 'Oracle' do plano [em tela].
 */
function renderOracle () {
  var deferred = new $.Deferred();
  _.delay(function () {
    if(!$el.find('.oracle').is(':visible')) {
      $el.find('.rule:visible').hide();
      $el.find('.oracle').fadeIn();
    }
    deferred.resolve();
  }, 500);
  return deferred.promise();
}

/*
 * Renderiza o 'Chaos' do plano [em tela].
 */
function renderChaos () {
  var deferred = new $.Deferred();
  _.delay(function () {
    if(!$el.find('.chaos').is(':visible')) {
      $el.find('.rule:visible').hide();
      $el.find('.chaos').fadeIn();
    }
    deferred.resolve();
  }, 500);
  return deferred.promise();
}

/*
 * Passo a passo de uma compra (interação com o library).
 */
function draw (action) {
  return exitPlane()
    .then(library[action])
    .then(library.getFaceUp)
    .then(triggerLayout)
    .then(renderCard)
    .then(enterPlane)
    .done(renderOracle);
}

function getBackCard () {
  var deferred = new $.Deferred();
  deferred.resolve(config.back);
  return deferred.promise();
}

/*
 * 
 */
function startGame () {
  return getBackCard()
    .then(renderCard)
    .then(enterPlane);
}

/*
 * Compra a próxima carta.
 */
function planeswalk () {
  return draw('next');
}

/*
 * Volta à carta do fundo do grimório.
 */
function rollback () {
  return draw('rollback');
}

/*
 * Lê o 'layout' da carta comprada e dispara 
 * um evento de mesmo nome.
 */
function triggerLayout (response) {
  $el.trigger(response[0].layout);
  return response;
}

function upsideDown (toggle) {
  $el.toggleClass('upsidedown', toggle);
  $el.toggleClass('upsideup', !toggle);
}

module.exports = function ($wrapper) {
  
  $el = $($wrapper);
  
  draw('next')
  // startGame()
    .then(function () { $el.trigger('load'); });

  return {
    $el: $el,
    rollback: rollback,
    planeswalk: planeswalk,
    oracle: renderOracle,
    chaos: renderChaos,
    upsideDown: upsideDown
  }
}