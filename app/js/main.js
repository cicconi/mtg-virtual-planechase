var $ = jQuery =    require('jquery');
var hammerjs =      require('jquery-hammerjs/jquery.hammer-full.min');
var dice =          require('./dice');
var battlefield =   require('./battlefield');

// http://wiki.mtgsalvation.com/article/Planechase_%28variant%29
// http://wiki.mtgsalvation.com/article/Command_zone

/*
 * TODO
 * # Implementar o Swiper (idangerous)
 * # Separar melhor conceitos entre módulos
 */

$(document).on('ready', function () {

  var planarDie = dice('#dice');
  var commandZone = battlefield('.plane-wrapper');

  window.dice = planarDie;
  window.cz = commandZone;

  // http://stackoverflow.com/questions/7768269/ipad-safari-disable-scrolling-and-bounce-effect
  document.ontouchmove = function (ev){
    ev.preventDefault();
  }

  // http://css-tricks.com/almanac/properties/p/pointer-events/
  function disableUI (toggle) {
    $('body').css('pointer-events', toggle? 'none' : 'auto');
  }

  planarDie.$el
    .hammer({ gesture: true })
    .on('tap',    function() { console.log('dice tapped'); planarDie.roll();  })
    .on('load',   function() { console.log('dice loaded'); })
    .on('roll',   function() { console.log('roll'); disableUI(true); })
    .on('stop',   function() { console.log('stop'); disableUI(false); })
    .on('chaos',  function() { console.log('chaos'); commandZone.chaos(); })
    .on('plane',  function() { console.log('plane'); commandZone.planeswalk(); })
    .on('blank',  function() { console.log('blank'); commandZone.oracle(); })

  commandZone.$el
    .hammer({ gesture: true })
    .on('load',       function() { console.log('battlefield loaded'); planarDie.hide(); })
    .on('phenomenon', function() { console.log('phenomenon drawn'); planarDie.hide(); })
    .on('plane',      function() { console.log('plane drawn'); planarDie.show(); })
    .on('tap',        function() { console.log('plane click'); commandZone.planeswalk(); })
    .on('swiperight', function() { console.log('plane swiperight'); commandZone.rollback(); })
    .on('swipeleft',  function() { console.log('plane swipeleft'); commandZone.planeswalk(); })
    .on('swipeup',    function() { console.log('plane swipeup'); commandZone.upsideDown(true); })
    .on('swipedown',  function() { console.log('plane swipedown');  commandZone.upsideDown(false); })

});

