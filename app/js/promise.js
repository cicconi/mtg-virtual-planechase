var promises = {};

function promise (name, func) {
  
  if ( typeof promises[name] === 'undefined' ) {
    promises[name] = func();
  }
  
  return promises[name];
}

module.exports = promise;