var _ = require('lodash');

module.exports = function (text) {
  
  var symbols = [
    { symbol :'R', value: 'red-mana' },
    { symbol :'G', value: 'green-mana' },
    { symbol :'B', value: 'black-mana' },
    { symbol :'U', value: 'blue-mana' },
    { symbol :'W', value: 'white-mana' },
    { symbol :'T', value: 'tap' },
    { symbol :'C', value: 'chaos' },
    { symbol :'P', value: 'plane' }
  ];
  var template = _.template('<span class="mtg-icon <%= symbol %>">&nbsp;</span>');
  var replaceSymbol = function (text, item) {
    var pattern = new RegExp('{'+ item.symbol +'}', 'g');
    return text.replace(pattern, template({ symbol: item.value }));
  };

  return _.reduce(symbols, replaceSymbol, text);
};