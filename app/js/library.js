var $ = jQuery =  require('jquery');
var _ =           require('lodash');
var promise =     require('./promise');
var config =      require('./config');

/*
 * Faz download das informações do set 
 * de Magic de Planechase (com cache).
 */
function getPlaneSet () {
  return promise('planesSet', function() {
    return $.ajax(config.route.planes);
  });
}

/*
 * Recebe um set de cartas e filtra apenas 
 * por Planes e Phenomenons.
 */
function filterPlanarCards (set) {
  return _(set.cards).filter(function (card) {
    return card.layout === 'plane' || card.layout === 'phenomenon';
  }).value();
}

/*
 * Recebe um set de cartas e embaralha.
 */
function shuffleLibrary (cards) {
  return _(cards).shuffle().value();
}

/*
 * Devolve um grimório de Planechase já 
 * embaralhado (e faz cache).
 */
function getLibrary () {
  return promise('library', function() {
    return getPlaneSet()
      .then(filterPlanarCards)
      .then(shuffleLibrary);
  });
}

/*
 * Coloca a carta que está no topo do grimório
 * no fundo.
 */
function next () {
  return getLibrary().pipe(function (cards) {
    cards.push(cards.shift());
    return cards;
  }); 
}

/*
 * Coloca a carta que está no fundo do grimório
 * no topo. (reverso de 'next')
 */
function rollback () {
  return getLibrary().pipe(function (cards) {
    cards.unshift(cards.pop());
    return cards;
  }); 
}

/*
 * Lê as informações da carta que está no topo
 * do grimório.
 */
function getFaceUp () {
  return getLibrary().pipe(function (cards) {
    return _(cards).take(2).value();
  }); 
}

// function getNextFace () {
//   return getLibrary().pipe(function (cards) {
//     return _(cards).take(2).value()[1];
//   }); 
// }

module.exports = function () {
  
  return {
    next: next,
    rollback: rollback,
    getFaceUp: getFaceUp//,
    // getNextFace: getNextFace
  }
}