var $ = jQuery =  require('jquery');
var _ =           require('lodash');
var promise =     require('./promise');
var Roulette =    require('./roulette');
var roulette;
var $el;
var config = {
  speed:        30,
  duration:     0.5,
  imgPath:      'img/icons/dice__',
  blankFace:    'blank',
  chaosFace:    'chaos',
  planeFace:    'plane',
  defaultDice:  ['blank', 'blank', 'plane', 'blank', 'blank', 'chaos']
};

/*
 * Joga o dado.
 * Desabilita o click enquanto o dado está
 * rolando.
 */
var start = function (ev) {
  $el.trigger('roll');
  roulette.start();
}

/*
 * Lê o dado quando o mesmo pára.
 * Reabilita o clique - para que o dado possa
 * ser lançado novamente.
 */
var stop = function($el) {
  $el.trigger('stop');
  $el.trigger( $el.data('name') );
}

/*
 * Template (html) de cada face do dado.
 */
var getFaceTemplate = function (type, options) {
  return _.template('<img data-name="<%=face%>" src="<%=imgPath%><%=face%>.png" />', _.extend({
    face: type || config.blankFace,
    imgPath: config.imgPath
  }, options));
}

/*
 * Template (html) do dado inteiro.
 */
var getDiceTemplate = function (symbols) {
  var wrapSymbol = function (acc, symbol) { 
    return acc + getFaceTemplate(symbol);
  }
  return _.reduce(_.shuffle(symbols), wrapSymbol, '');
}

/*
 * Retorna o markup do dado em forma de promise.
 */
var getDice = function (options) {
  return promise('dice', function () {
    var defer = new $.Deferred();
    _.delay(function(){
      defer.resolve( getDiceTemplate( options.faces ) );
    },0);
    return defer.promise();
  });
}

var hideDice = function () {
  if($el.is(':visible')) {
    $el.fadeOut();
  }
}

var showDice = function () {
  if(!$el.is(':visible')) {
    $el.fadeIn();
  }
}

module.exports = function ($wrapper, options) {
  
  $el = $($wrapper);

  options = _.defaults({
    faces : config.defaultDice,
    speed : config.speed,
    duration : config.duration,
    stopCallback : stop
  }, options);

  roulette = new Roulette(options);

  getDice(options).done( function (response) {
    $el
      .append(response)
      // .on('click', start)
      .trigger('load');
    
    roulette.init($el);
  });
  
  return {
    $el: $el,
    roll: start,
    hide: hideDice,
    show: showDice
  };
}