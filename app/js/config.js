module.exports = {
  route: {
    template: 'plane.template.html',
    planes: 'json/mtgset-HOP.json'
  },

  back: {
    "text" : "",
    "rarity" : "",
    "type" : "",
    "number" : "0",
    "artist" : "",
    "subtypes" : [],
    "name" : "",
    "types" : [],
    "layout" : "back",
    "imageName" : "",
    "multiverseid" : 0
  }
}